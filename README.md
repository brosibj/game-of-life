# Game Of Life #

This is a clone of Conway's Game of Life.

## Features ##

* Uses torus board logic for wrapping around edges.
* Customized sizes
* Four Template boards
* Randomized boards

## Planned ##

* Custom in-file for board layout.

## How to use ##

Just [download the .jar](https://bitbucket.org/Fearlessagent/game-of-life/downloads/Game%20Of%20Life.jar?pk=402521) and run it. Should work out of the gate. If not, update java.
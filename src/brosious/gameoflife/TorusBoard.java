package brosious.gameoflife;

import java.util.ArrayList;

public class TorusBoard {
	public static final int NORTH_WEST = 0;
	public static final int NORTH = 1;
	public static final int NORTH_EAST = 2;
	public static final int WEST = 3;
	public static final int EAST = 5;
	public static final int SOUTH_WEST = 6;
	public static final int SOUTH = 7;
	public static final int SOUTH_EAST = 8;

	private int height, width;
	private ArrayList<ArrayList<TorusTile>> board = new ArrayList<ArrayList<TorusTile>>(0);

	/**
	 * Instantiate TorusBoard with height and width. Randomizes starting grid.
	 * @param height
	 * @param width
	 */
	public TorusBoard(int h, int w) {
		height = h;
		width = w;
		ArrayList<TorusTile> innerList = new ArrayList<TorusTile>(0);
        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                innerList.add(new TorusTile());
            }
            board.add(innerList);
            innerList = new ArrayList<TorusTile>(0);
        }
	}
	
	public TorusBoard(int h, int w, ArrayList<ArrayList<TorusTile>> arr){
		height = h;
		width = w;
		board = arr;	
	}
	
	public void setBoard(ArrayList<ArrayList<TorusTile>> arr){this.board = arr;}

	/**
	 * Sets tile state (type) in grid.
	 * @param x_coord
	 * @param y_coord
	 * @param state
	 */
	public void setTile(int x, int y, int state){board.get(y).get(x).setType(state);}
	/**
	 * returns integer of board height.
	 * @return<i>int</i> height
	 */
	public int height() {return this.height;}
	/**
	 * Returns integer of board width.
	 * @return <i>int</i> width
	 */
	public int width() {return this.width;}
	/**
	 * Returns raw data <i>ArrayList</i>.
	 * @return <i>ArrayList</i> board
	 */
	public ArrayList<ArrayList<TorusTile>> getBoard(){return this.board;}
	/**
	 * Returns TorusTile object.
	 * @param x_coord
	 * @param y_coord
	 * @return <i>TorusTile</i> tile
	 */
	public TorusTile getTile(int x, int y){return this.board.get(y).get(x);}
	/**
	 * Get the neighboring cell in given direction starting at given Tile's coordinates.
	 * @param dir
	 * @param oX
	 * @param oY
	 * @return <i>TorusTile</i> tile
	 */
	public TorusTile getNeighbor(int dir, int oX, int oY){		
		int dx = (dir % 3) - 1;
		int dy = (int) (dir / 3) - 1;
		
		int newX = oX + dx;
		int newY = oY + dy;
		
		if (newX < 0) newX = this.width-1;
		else if (newX > this.width-1) newX = 0;
		if (newY < 0) newY = this.height-1;
		else if (newY > this.height-1) newY = 0;
		
		return this.board.get(newY).get(newX);
	}

	/**
	 * Get number of neighbors in given state around given coordinates.
	 * @param state
	 * @param oX
	 * @param oY
	 * @return <i>int</i> number
	 */
	public int getNeighborsInState(int state, int oX, int oY){

		int neighborsState = 0;
		if (this.getNeighbor(TorusBoard.EAST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.WEST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.NORTH, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.SOUTH, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.NORTH_EAST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.SOUTH_EAST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.NORTH_WEST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.SOUTH_WEST, oX, oY).type() == state){
			neighborsState++;
		}
		return neighborsState;
	}
}
